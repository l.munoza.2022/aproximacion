
def base_tree():

    while True:
        try:
            tree = int(input("Introduce el nº de arboles base: "))
            fruts_tree = int(input("Introduce el nº de frutas por arboles: "))
            reduction = int(input("Introduce la reduccion: "))
            minimun = int(input("Introduce el minimo de arboles: "))
            maximun = int(input("Introduce el maximo de arboles: "))
            return tree, fruts_tree, reduction, maximun, minimun
        except ValueError:
            print("Este dato no es valido")


t, f ,r, maxi, min = base_tree()


def comprobar_datos(t, f, r, maxin, min):

    while min > maxin or min < t or r<0:
        if min > maxin:
            print("Introduce un minimo menor que el maximo")
            t, f, r, maxin, min = base_tree()
        elif min < t:
            print("Introduce un minimo mayor que el nº de arboles base")
            t, f, r, maxin, min = base_tree()
        else:
            print("Introduce una reduccion mayor que 0")
            t, f, r, maxin, min = base_tree()
    return t, f, r, maxin, min


tre, fruit, red, maxin, min = comprobar_datos(t, f ,r, maxi, min)


def main(tre, fruit, red, maxi, min):
    lista = list(range(min,int(maxi)+1))
    prod = []
    it = []
    for item in lista:
        produccion = (fruit -((item - tre)*red))*item
        prod.append(produccion)
        it.append(item)
        print(f'{item}' f'\t{produccion}')

    maximo=max(prod)
    a = prod.index(maximo)
    print(f'La mejor produccion es {it[a]}  con' f'\t{maximo}')


main(tre, fruit, red, maxin, min)