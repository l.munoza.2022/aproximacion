import sys


def compute_trees(trees):
    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production


def compute_all(min_trees, max_trees):
    productions = []

    for trees in range(min_trees, max_trees + 1):
        production = compute_trees(trees)
        productions.append((trees, production))
    return productions


def read_arguments():
    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])
    except IndexError:
        sys.exit("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")
    except ValueError:
        sys.exit("All arguments must be integers")

        if len(sys.argv) > 6:
            sys.exit("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")

        return base_trees, fruit_per_tree, reduction, min, max

    def main():
        global base_trees
        global fruit_per_tree
        global reduction

        best_production = 0
        best_trees = 0
        base_trees, fruit_per_tree, reduction, min, max = read_arguments()

        productions = compute_all(min, max)

        for product_tree in productions:
            trees = product_tree[0]
            production = product_tree[1]
            print(trees, production)
            if production > best_production:
                best_trees = trees
                best_production = production
        print(f"Best production: {best_production}, for {best_trees} trees")

    if __name__ == '__main__':
        main()
